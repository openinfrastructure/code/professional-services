Google Private Catalog Examples
===

This directory contains reusable solutions for publication in [Google
Private Catalog][1]. Each directory represents a unique solution, and each
solution will contain its own README file for documentation.

[1]: https://cloud.google.com/private-catalog/
