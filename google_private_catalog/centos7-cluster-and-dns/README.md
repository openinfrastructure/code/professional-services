Centos 7 Cluster + DNS Private Catalog Solution
===

This Private Catalog + Deployment Manager template provides a self-service
solution for spinning up a cluster of Centos 7 VMs, including DNS entries
into a managed zone if desired, based on the data provided to the Catalog. 

Within this directory is contained DM templates, schema files, and build
scripts used to produce azip file suitable for publication within [Private Catalog][1].

 * [configurations](configurations/) holds top level Deployment Manager
   configuration files intended for use as examples outside of Private Catalog.
 * [templates](templates/) holds re-usable DM templates intended to be layered
   and built up into high level solutions.

Configuration Changes
===

Before building and deploying the solution, there are configuration changes
that need to be made to tailor the template to your environment:

### VPC and DNS Information

Within [centos7vm.py](./templates/centos7vm.py) there are a series of
environment-specific constant values that need to be set based on the DNS
Zone and DNS/VPC project names being used for nonprod and prod. That section
currently looks like the following:


```
# Environment-specific constants (change these based on your ENV)
NONPROD_DNS_ZONE = 'nonprod-private-zone'
NONPROD_DNS_PROJECT = 'dnsregistration'
NONPROD_VPC_HOST_PROJECT = 'dnsregistration'
PROD_DNS_ZONE = 'prod-private-zone'
PROD_DNS_PROJECT = 'dnsregistration'
PROD_VPC_HOST_PROJECT = 'dnsregistration
```

The values present are the values we initially used for testing, but they
should be changed to the names of the DNS zones, the names of the projects
hosting the DNS zones, and the names of the Shared VPC host projects where
subnetworks resources are located for both prod and nonprod deployment
environments. The deployment template code assumes a nonprod/prod split for all functionality, so if that assumption is not correct then further modifications will need to be made to the conditional logic within [`centos7vm.py`](./templates/centos7vm.py).


### Subnetwork Names

Unfortunately as of the time of this writing (06/2019) there's no way to
dynamically populate a dropdown box based on current environment data (i.e.
populating a dropdown list of live subnetwork data), so we have to manually
provide that data within
[`centos7cluster.py.schema`](./templates/centos7cluster.py.schema). If your
subnetwork names are identical for prod/nonprod then that dropdown box is
easy to populate, but if you're using different subnetwork names then you'll
need to add some more conditional logic to determine the exact subnetwork
that should be used. This code should be used as a starting point for that
process. The goal for the current code layout was to eliminate the possiblity
of selecting a `nonprod` deployment environment and a `prod` subnetwork (or
vice-versa), so that's why the subnetwork names aren't as flexible as you
might need.


Build and Publish a Solution
===

To publish the solution, zip the templates folder and name the zip file the same
as the entry point.  The main entry point of this example is `centos7cluster`, so
produce and publish `centos7cluster.zip`.  This zip file will present a UI based
on `centos7cluster.py.schema` and use `centos7cluster.py` as the main entry point.

```bash
(cd templates/; zip ~/Downloads/centos7cluster.zip *)
```

Expected output:

```
updating: centos7cluster.py (deflated 49%)
updating: centos7cluster.py.schema (deflated 72%)
updating: centos7vm.py (deflated 72%)
updating: helpers.py (deflated 47%)
```

Use Cases
===

This folder is intended for example code demonstrating DNS registration for the
following customer use cases:

 1. [x] Use Case 1 - Automatic Deletion of DNS A Records created via Private Catalog
 2. [x] Use Case 2 - Support DNS A Record creation via Private Catalog
 3. [x] Use Case 3 - Support DNS being present on first boot.  Also reboots.

[1]: https://cloud.google.com/private-catalog/
