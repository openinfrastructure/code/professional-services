Configurations
===

This folder contains top level Deployment Manager Configurations.  These are
intended to exercise the templates outside of Private Catalog.

`centos7cluster.yaml` demonstrates a 3 node CentOS 7 cluster.  Each node in the
cluster is modeled using the `centos7vm.py` template.  The `centos7vm.py` template
is responsible for modeling a single VM instance and is the recommended
location for the association of DNS records to VM instances.

To deploy the cluster:

```bash
gcloud deployment-manager deployments create --config centos7cluster.yaml resourcetest
```

Expected output:

```
The fingerprint of the deployment is z8SeYtZik9791z5l8ahgcg==
Waiting for create [operation-1561140007067-58bd93b90ded6-9f7472bc-498d3710]...done.
Create operation operation-1561140007067-58bd93b90ded6-9f7472bc-498d3710 completed successfully.
NAME                        TYPE                                 STATE      ERRORS  INTENT
resourcetest001             compute.v1.instance                  COMPLETED  []
resourcetest001-dns-create  gcp-types/dns-v1:dns.changes.create  COMPLETED  []
resourcetest001-dns-delete  gcp-types/dns-v1:dns.changes.create  COMPLETED  []
resourcetest001-ipaddress   gcp-types/compute-v1:addresses       COMPLETED  []
resourcetest002             compute.v1.instance                  COMPLETED  []
resourcetest002-dns-create  gcp-types/dns-v1:dns.changes.create  COMPLETED  []
resourcetest002-dns-delete  gcp-types/dns-v1:dns.changes.create  COMPLETED  []
resourcetest002-ipaddress   gcp-types/compute-v1:addresses       COMPLETED  []
```
