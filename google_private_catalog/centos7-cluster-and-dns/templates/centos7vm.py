"""Manage a CentOS 7 VM"""

from helpers import GenerateFQDN
COMPUTE_URL_BASE = 'https://www.googleapis.com/compute/v1/'

# Environment-specific constants (change these based on your ENV)
NONPROD_DNS_ZONE = 'nonprod-private-zone'
NONPROD_DNS_PROJECT = 'dnsregistration'
NONPROD_VPC_HOST_PROJECT = 'dnsregistration'
PROD_DNS_ZONE = 'prod-private-zone'
PROD_DNS_PROJECT = 'dnsregistration'
PROD_VPC_HOST_PROJECT = 'dnsregistration'


def GlobalComputeUrl(project, collection, name):
    """Generate global compute URL."""
    return ''.join([COMPUTE_URL_BASE, 'projects/', project, '/global/',
                    collection, '/', name])


def ZonalComputeUrl(project, zone, collection, name):
    """Generate zone compute URL."""
    return ''.join([COMPUTE_URL_BASE, 'projects/', project, '/zones/', zone,
                    '/', collection, '/', name])


def RegionalComputeUrl(project, region, collection, name):
    """Generate regional compute URL."""
    return ''.join([COMPUTE_URL_BASE, 'projects/', project, '/regions/',
                    region, '/', collection, '/', name])

def CpuPlatform(instance_type):
    """Determine CPU platform based on instance type."""
    if instance_type == 'n1-highmem-32':
        return "Intel Broadwell"
    elif instance_type == 'n1-highmem-64':
        return "Intel Broadwell"
    elif instance_type == 'n1-highmem-96':
        return "Intel Skylake"
    elif instance_type == 'n1-megamem-96':
        return "Intel Skylake"
    elif instance_type == 'n1-ultramem-40':
        return "Automatic"
    elif instance_type == 'n1-ultramem-80':
        return "Automatic"
    elif instance_type == 'n1-ultramem-160':
        return "Automatic"
    else:
        return "Automatic"


def GenerateConfig(context):
    """Generate configuration."""

    # Get/generate variables from context
    zone = context.properties['Zone']
    project = context.env['project']
    instance_name = context.env['name']
    instance_type = ZonalComputeUrl(project, zone, 'machineTypes',
                                    context.properties['Instance Type'])
    region = context.properties['Zone'][:-2]
    core_image = context.properties['Image']
    deploy_env = context.properties['Deploy Environment']
    subnetwork_name = context.properties['Subnetwork Name']
    service_account = str(
        context.properties.get(
            'Service Account',
            '%s-compute@developer.gserviceaccount.com'
            % context.env['project_number']))

    # Compile network tags based on comma separated list
    if len(str(context.properties.get('Network Tag', ''))):
        items = str(context.properties.get('Network Tag', '')).split(',')
    else:
        items = []
    network_tags = {"items": items}

    # Get deployment template specific variables from context
    cluster_nodes = int(context.properties.get('Cluster Nodes',
                                                     '1'))
    post_deployment_script = str(context.properties.get('Startup Script', ''))

    # Determine VPC project, DNS zone, and DNS project based on deploy_env
    if deploy_env == 'prod':
        vpc_project = PROD_VPC_HOST_PROJECT
        dns_zone = PROD_DNS_ZONE
        dns_project = PROD_DNS_PROJECT
    else:
        vpc_project = NONPROD_VPC_HOST_PROJECT
        dns_zone = NONPROD_DNS_ZONE
        dns_project = NONPROD_DNS_PROJECT
    subnetwork = RegionalComputeUrl(vpc_project, region, 'subnetworks',
                                        subnetwork_name)

    # Delete Protection
    if str(context.properties['Delete Protection']) == "False":
        deleteProtection = False
    else:
        deleteProtection = True

    # Determine instance CPU platform
    cpu_platform = CpuPlatform(context.properties['Instance Type'])

    # Compile networking
    networking = []

    # determine disk sizes
    if 'projects/centos-cloud' in core_image:
        osDiskSizeGB = '10'
    else:
        osDiskSizeGB = '50'

    # compile complete json for resources and outputs
    # https://cloud.google.com/deployment-manager/docs/configuration/expose-information-outputs
    resources = []
    outputs = []

    # The VM resource, may be modified later, e.g. if DNS is being registered.
    vm_resource = {
        'name': instance_name,
        'type': 'compute.v1.instance',
        'properties': {
            'zone': zone,
            'deletionProtection': deleteProtection,
            'minCpuPlatform': cpu_platform,
            'machineType': instance_type,
            'metadata': {
                'items': [
                    {
                        'key': 'startup-script-url',
                        'value': post_deployment_script
                    },
                    {
                        'key': 'resourceowner',
                        'value': context.properties['Owner']
                    }
                ]
            },
            "tags": network_tags,
            "labels": {
                "resource-owner": context.properties['Owner'],
                "deploy-environment": deploy_env,
            },
            'disks': [{
                'deviceName': 'boot',
                'type': 'PERSISTENT',
                'autoDelete': True,
                'boot': True,
                'initializeParams': {
                    'diskName': instance_name + '-boot',
                    'sourceImage': core_image,
                    'diskSizeGb': osDiskSizeGB
                }
            }],
            'canIpForward': True,
            'serviceAccounts': [{
                'email': service_account,
                'scopes': [
                    'https://www.googleapis.com/auth/cloud-platform'
                ]
            }],
            'networkInterfaces': [{
                'accessConfigs': networking,
                'subnetwork': subnetwork
            }]
        }
    }

    # Determine DNS requirements
    if context.properties['Register DNS']:
        fqdn = GenerateFQDN(instance_name, deploy_env, region)
        network_ip = '$(ref.%s-ipaddress.address)' % instance_name
        vm_resource['properties']['networkInterfaces'][0]['networkIP'] \
            = network_ip

        # Statically reserve the IP address resource so the DNS entry can be
        # tied to its address
        resources.append({
             'name': instance_name + '-ipaddress',
             'type': 'gcp-types/compute-v1:addresses',
             'properties': {
                 'addressType': 'INTERNAL',
                 'region': region,
                 'subnetwork': subnetwork
             }
         })

        # The IP addresses used for A Records.  In normal situations this is a
        # single element array to have the A record resolve to a single IP.
        rrdatas = [network_ip]
        dns_record = {
            'name': fqdn,
            'type': 'A',
            'ttl': 300,
            'rrdatas': rrdatas
        }

        # There's an open bug where the 'gcp-types/dns-v1:resourceRecordSets'
        # type has issues creating a DNS record within a Shared VPC host
        # project from Private Catalog running in the service project. The
        # resources below work around that by using the changes API, but that
        # also has an issue:
        # https://github.com/GoogleCloudPlatform/deploymentmanager-samples/issues/62
        # This is mitigated by using separate resources for each runtime policy
        # to ensure Private Catalog can both create and delete the records.
        resources.append({
            'name': instance_name + '-dns-create',
            'action': 'gcp-types/dns-v1:dns.changes.create',
            'metadata': {
                'runtimePolicy': [
                    'CREATE',
                ],
            },
            'properties': {
                'project': dns_project,
                'managedZone': dns_zone,
                'additions': [dns_record]
            }
        })

        resources.append({
            'name': instance_name + '-dns-delete',
            'action': 'gcp-types/dns-v1:dns.changes.create',
            'metadata': {
                'runtimePolicy': [
                    'DELETE',
                ],
            },
            'properties': {
                'project': dns_project,
                'managedZone': dns_zone,
                'deletions': [dns_record]
            }
        })

    # Collect all resources and outputs
    resources.append(vm_resource)
    outputs.append({'name': instance_name + '-fqdn', 'value': fqdn})

    return {'resources': resources, 'outputs': outputs}
