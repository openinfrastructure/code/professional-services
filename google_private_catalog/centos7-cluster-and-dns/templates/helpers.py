"""Helper functions intended for re-use within templates"""


def GenerateFQDN(shortname, deploy_env, region):
    """Generate a FQDN from user-provided inputs.  This helper method is
    maintained by the core platform team and used broadly by application
    deployment scripts.

    Args:
      shortname: A short hostname string, e.g. "www1"
      deploy_env: The deployment environment string, e.g. "prod" or "nonprod"
      region: The region being deployed into, e.g. "us-west2"
    Returns:
      The fqdn string, e.g. "www1.us-west2.nonprod.gcp.example.com"
    """

    # Note the trailing dot, which is important for the FQDN when used in a DNS
    # Record Set.
    BASE = "gcp.example.com."
    return "%s.%s.%s.%s" % (shortname, region, deploy_env, BASE)
