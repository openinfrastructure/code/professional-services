"""Create one or more CentOS 7 nodes in a cluster."""


def GenerateConfig(context):
    """Generate configuration."""

    # Get deployment template specific variables from context
    cluster_nodes = int(context.properties.get(
        'Cluster Nodes', '1'))

    resources = []

    for i in range(0, cluster_nodes):

        instance_name = context.properties['Instance Name']
        instance_name += str(i+1).zfill(3)

        resources.append({
            'name': instance_name,
            'type': 'centos7vm.py',
            'properties': context.properties
        })

    return {'resources': resources}
