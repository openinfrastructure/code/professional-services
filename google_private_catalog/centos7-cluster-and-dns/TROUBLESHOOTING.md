# Troubleshooting

This document captures error messages and their solutions.

# Access Not Configured

## Description

Cloud DNS has not yet been configured in the target project.

## Solution

Enable Cloud DNS API.

## Context / Steps to Reproduce

Given a new project without Cloud DNS API enabled or used, try to create a
named VM.

```
gcloud deployment-manager deployments create named-vm --config named-vm.yaml
```

## Error Message


```
The fingerprint of the deployment is Rb3CZEFi_i91ne7xeAVRTw==
Waiting for create [operation-1558723861215-589a6ae0cf899-7d77a306-bec35aad]...failed.
ERROR: (gcloud.deployment-manager.deployments.create) Error in Operation [operation-1558723861215-589a6ae0cf899-7d77a306-bec35aad]: errors:
- code: RESOURCE_ERROR
  location: /deployments/named-vm/resources/named-vm-rr
  message: '{"ResourceType":"gcp-types/dns-v1:resourceRecordSets","ResourceErrorCode":"403","ResourceErrorMessage":{"code":403,"errors":[{"domain":"usageLimits","message":"Access
    Not Configured. Google Cloud DNS API has not been used in project 778092399017
    before or it is disabled. Enable it by visiting https://console.developers.google.com/apis/api/dns.googleapis.com/overview?project=778092399017
    then retry. If you enabled this API recently, wait a few minutes for the action
    to propagate to our systems and retry.","reason":"accessNotConfigured","extendedHelp":"https://console.developers.google.com/apis/api/dns.googleapis.com/overview?project=778092399017"}],"message":"Access
    Not Configured. Google Cloud DNS API has not been used in project 778092399017
    before or it is disabled. Enable it by visiting https://console.developers.google.com/apis/api/dns.googleapis.com/overview?project=778092399017
    then retry. If you enabled this API recently, wait a few minutes for the action
    to propagate to our systems and retry.","statusMessage":"Forbidden","requestPath":"https://www.googleapis.com/dns/v1/projects/dnsregistration/managedZones/us-west2-c/changes","httpMethod":"POST"}}'
```

# Managed Zone does not exist

## Description

The specified Private DNS [Managed Zone][managed-zone] does not exist when
attempting to deploy a named-vm.

## Solution

Create the correct [Managed Zone][managed-zone] in Cloud DNS.

## Context / Steps to Reproduce

Given a new project with Cloud DNS API enabled, try to create a named VM.

```
gcloud deployment-manager deployments create named-vm --config named-vm.yaml
```

## Error Message


```
Waiting for create [operation-1558733414333-589a8e775fc6b-8b719a82-b28cffd7]...failed.
ERROR: (gcloud.deployment-manager.deployments.create) Error in Operation [operation-1558733414333-589a8e775fc6b-8b719a82-b28cffd7]: errors:
- code: RESOURCE_ERROR
  location: /deployments/named-vm/resources/named-vm-rr
  message: "{\"ResourceType\":\"gcp-types/dns-v1:resourceRecordSets\",\"ResourceErrorCode\"\
    :\"404\",\"ResourceErrorMessage\":{\"code\":404,\"errors\":[{\"domain\":\"global\"\
    ,\"message\":\"The 'parameters.managedZone' resource named 'nonprod' does not\
    \ exist.\",\"reason\":\"notFound\"}],\"message\":\"The 'parameters.managedZone'\
    \ resource named 'nonprod' does not exist.\",\"statusMessage\":\"Not Found\",\"\
    requestPath\":\"https://www.googleapis.com/dns/v1/projects/dnsregistration/managedZones/nonprod/changes\"\
    ,\"httpMethod\":\"POST\"}}"
```

# Invalid value for 'entity.change.additions[0].name'

## Description

An invalid name is passed as a property to `gcp-types/dns-v1:resourceRecordSets`.

## Solution

Pass a fully qualified domain name to the `gcp-types/dns-v1:resourceRecordSets`
type.  This should be handled internally by the named-vm type, so this error
may indicate a regression in the `named-vm` implementation.

See also [deploymentmanager-samples][one_a_record_sample] for a valid example
of how to set an A record.

## Context / Steps to Reproduce

Given a project with a DNS Managed Zone, try to create a named VM.

```
gcloud deployment-manager deployments create named-vm --config named-vm.yaml
```

## Error Message


```
Waiting for create [operation-1558734418771-589a923547e60-7ba13c9b-e451a8b7]...failed.
ERROR: (gcloud.deployment-manager.deployments.create) Error in Operation [operation-1558734418771-589a923547e60-7ba13c9b-e451a8b7]: errors:
- code: RESOURCE_ERROR
  location: /deployments/named-vm/resources/named-vm-rr
  message: "{\"ResourceType\":\"gcp-types/dns-v1:resourceRecordSets\",\"ResourceErrorCode\"\
    :\"400\",\"ResourceErrorMessage\":{\"code\":400,\"errors\":[{\"domain\":\"global\"\
    ,\"message\":\"Invalid value for 'entity.change.additions[0].name': 'named-vm-rr'\"\
    ,\"reason\":\"invalid\"}],\"message\":\"Invalid value for 'entity.change.additions[0].name':\
    \ 'named-vm-rr'\",\"statusMessage\":\"Bad Request\",\"requestPath\":\"https://www.googleapis.com/dns/v1/projects/dnsregistration/managedZones/nonprod/changes\"\
    ,\"httpMethod\":\"POST\"}}"
```

# Referenced resource could not be found

## Description

A [resource reference][resource-reference] refers to a resource that cannot be
found.

## Solution

Align the resource reference with the correct resource name.  This is likely an
issue internally within the `named-vm` type and may indicate a regression.

## Context / Steps to Reproduce

Given a project with a DNS Managed Zone, try to create a named VM.

```
gcloud deployment-manager deployments create named-vm --config named-vm.yaml
```

## Error Message

```
Waiting for create [operation-1558735311388-589a95888c020-303d7dd5-8d0cc84d]...failed.
ERROR: (gcloud.deployment-manager.deployments.create) Error in Operation [operation-1558735311388-589a95888c020-303d7dd5-8d0cc84d]: errors:
- code: CONDITION_NOT_MET
  message: Referenced resource somevm-vm could not be found. At resource somevm-rr.
```

# Invalid value for 'entity.change.additions[0].name'

## Description

The error is likely a result of the DNS Record Set's `name` being outside the
Managed Zone's domain.

## Solution

Make sure the fully qualified domain name passed is within the DNS Managed
Zone's domain.  For example:

```diff
diff --git a/dns/deployment_configurations/named_vm/named-vm.yaml b/dns/deployment_configurations/named_vm/named-vm.yaml
index a35cf02..80d2f54 100644
--- a/dns/deployment_configurations/named_vm/named-vm.yaml
+++ b/dns/deployment_configurations/named_vm/named-vm.yaml
@@ -10,6 +10,6 @@ resources:
 - name: somevm
   type: named_vm.py
   properties:
-    fqdn: vm1.nonprod.example.com.
+    fqdn: vm1.nonprod.gcp.example.com.
     computeZone: us-west2-c
     managedZone: nonprod
```

## Context / Steps to Reproduce

Given a project with a DNS Managed Zone, try to create a named VM.

```
gcloud deployment-manager deployments create named-vm --config named-vm.yaml
```

## Error Message

```
Waiting for create [operation-1558737002273-589a9bd519aed-a43afbaf-2a46c9af]...failed.
ERROR: (gcloud.deployment-manager.deployments.create) Error in Operation [operation-1558737002273-589a9bd519aed-a43afbaf-2a46c9af]: errors:
- code: RESOURCE_ERROR
  location: /deployments/named-vm/resources/somevm-rr
  message: "{\"ResourceType\":\"gcp-types/dns-v1:resourceRecordSets\",\"ResourceErrorCode\"\
    :\"400\",\"ResourceErrorMessage\":{\"code\":400,\"errors\":[{\"domain\":\"global\"\
    ,\"message\":\"Invalid value for 'entity.change.additions[0].name': 'vm1.nonprod.example.com.'\"\
    ,\"reason\":\"invalid\"}],\"message\":\"Invalid value for 'entity.change.additions[0].name':\
    \ 'vm1.nonprod.example.com.'\",\"statusMessage\":\"Bad Request\",\"requestPath\"\
    :\"https://www.googleapis.com/dns/v1/projects/dnsregistration/managedZones/nonprod/changes\"\
    ,\"httpMethod\":\"POST\"}}"
```

# MANIFEST_EXPANSION_USER_ERROR

## Description

When trying to use a Python helper method, deployments fail with a message
similar to the following.  This is caused by the Python environment running in
GCP failing to load the helper method.  Helper methods need to be loaded from
the top level configuration file, i.e. the yaml file passed to the `gcloud
deployment-manager deployments` command so they are bundled up into the
deployment and sent to the API for expansion.

## Solution

Take care to import the helper methods from the top level configuration.  For example, in the top level YAML config file, import using:

```yaml
imports:
- path: ../templates/helpers/fqdn.py
  name: helpers/fqdn.py
```

Given this named import, Python templates are able to use the helper methods like so:

```python
from helpers.fqdn import GenerateFQDN

fqdn = GenerateFQDN(shortname, deploy_env, region)
```

## Context / Steps to Reproduce

Given a configuration which has not imported the helper methods from the top level configuration.

```
gcloud deployment-manager deployments create multi-vm --config multi-vm.yaml
```

## Error Message

```
ERROR: (gcloud.deployment-manager.deployments.create) Error in Operation [operation-1559084347605-589fa9cb6c266-91f665d6-cc405198]: errors:
- code: MANIFEST_EXPANSION_USER_ERROR
  location: /deployments/multi-vm/manifests/manifest-1559084347696
  message: 'Manifest expansion encountered the following errors: Error compiling Python
    code: No module named helpers Resource: named_vm2.py Resource: config'
```

# Main template file not found

## Description

When trying to Add Deployment Manager template to Private Catalog, the form
rejects the zip file with the error, "Fail to create a new version for product
Multi VM Application.  Main template file not found."

As per [Private Catalog - DM Based Solution][dm].  "The name of the zip file
must match the names of both the schema and Jinja files."

## Solution

Ensure the name of the template file inside the zip file matches the zip
filename.  For example, if the zip file is named android-dev-environment.zip,
the schema file inside the zip must be named
android-dev-environment.jinja.schema and the main template must be named
android-dev-environment.jinja

## Context / Steps to Reproduce

Produce a zip file for upload using `multi_vm/build.sh`.  Upload multi-vm.zip.
Observe the error.

## Error Message

```
Fail to create a new version for product Multi VM Application.  Main template
file not found.
```

[managed-zone]: https://cloud.google.com/dns/zones/
[one_a_record_sample]: https://github.com/GoogleCloudPlatform/deploymentmanager-samples/blob/master/google/resource-snippets/dns-v1/one_a_record.jinja
[resource-reference]: https://cloud.google.com/deployment-manager/docs/step-by-step-guide/using-references
[dm]: https://cloud.google.com/private-catalog/docs/dm-based-solution
