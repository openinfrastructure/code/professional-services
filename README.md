Open Infrastructure Services Professional Services Repository
===

This repository contains open-sourced tools and solutions created and used
for Open Infrastructure Services professional services engagements. Top-level
directories delineate the technology, and subdirectories contain example
solutions each with their own `README` files describing the intent and
implementation of each solution.

